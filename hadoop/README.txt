***To Be Done First***
Set an enviroment variable:
HADOOP_HOME to point to the full path of hadoop-2.7.1
(This is system dependent. Find the way to do that in your system.)

Also, make sure you are using Java 8. 


Now open a terminal (Mac/Linux) or cmd (Windows). 

To use the programs in "hadoop_examples_post" do: 

to compile:
cd hadoop_examples_post
javac -cp "../spark-2.1.0-bin-hadoop2.7/jars/*" -d bin src/*.java


to run the program: 
  Mac/Linux
java -cp "../spark-2.1.0-bin-hadoop2.7/jars/*":bin MovielensAvg
  Windows:
java -cp "../spark-2.1.0-bin-hadoop2.7/jars/*";bin MovielensAvg
(can also use MovielensGenreAvg to check genre ratings)

to view results check output_movielens.


To use the programs in "SparkJava8Examples_post" do:

to compile:
cd SparkJava8Examples_post
javac -cp "../spark-2.1.0-bin-hadoop2.7/jars/*" -d bin src/*.java
 (Older version of Windows: change / to \)

to run the program: 
 Mac/Linux:
java -cp "../spark-2.1.0-bin-hadoop2.7/jars/*":bin SparkMovielensAvg
 Windows:
java -cp "../spark-2.1.0-bin-hadoop2.7/jars/*";bin SparkMovielensAvg
(can also use SparkMovielensGenreAvg and MedlineAnalysis_post)
to view result check output_movielens.
 
Remarks. 
1. Use Java 8.  

2. This was done as an assignment in Imir Thomo's SENG 480A class. The templates are  his work. 
