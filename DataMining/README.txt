For 2PMMS:
Compile and run GenerateData.java to output randomized information about people, including SIN number, name (based on popular-first.txt and popular-last.txt) and salary. 

Afterwards compile and run main1.java to run 2 phase multiway merge sort, check how fast it is, and compare with the system's sort method/speed (UNIX sort). Options available in main1 to change what is used to sort (i.e. SIN, name, or salary).

For textclassifier in naivebayes:
This takes train_data.txt as input in order to train the classifier. After the training is completed (extract features of each sentences, find their classification, update probablity) bernoulli is applied to training data (to make sure the classifier is working normally) and test data. out_text.txt and out_train.txt are the results of the operations.

Perceptron/Regression:
Full details contained in their rescriptive scripts.
