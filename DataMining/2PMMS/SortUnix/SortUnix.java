package SortUnix;
public class SortUnix {

	public static double main() throws Exception {
		long startTime = System.currentTimeMillis();
		
        System.out.println("System's sort beginning");
		//Remove ".exe" for Mac and Linux
        Runtime.getRuntime().exec("sort -k 2 taxpayers_3M.txt -o taxpayers_3M_sort.txt").waitFor();
        
		System.out.println("Time elapsed (sec) = " + (System.currentTimeMillis() - startTime) / 1000.0);
	    
        return (System.currentTimeMillis() - startTime) / 1000.0;
    }
}
