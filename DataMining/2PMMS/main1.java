import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Comparator;
import SortUnix.SortUnix;

public class main1 {
    
    public static void main(String[] args) throws Exception{
		

        int total = 3000000;//total number of records
        
        int M = 300000;//size of chunk
        int B = 8192;//size of buffer in characters
        int C = 0;//column to sort on
        

        //start phase 1 
        long startTime = System.currentTimeMillis();
        System.out.println("User implementation of 2PMMS beginning");
        System.out.println("Phase 1 started");

        int mx = (int) Math.ceil(total / M);
        int numChunks = phase1(mx, M, C);        
        
        double total1 = (System.currentTimeMillis() - startTime) / 1000.0;
        System.out.println("Phase 1 Time elapsed (sec) = " + total1);
        ///phase 1 done///
        

        startTime = System.currentTimeMillis();
        
        System.out.println("Phase 2 started");
        phase2(numChunks, B, C);
        total1 += (System.currentTimeMillis() - startTime) / 1000.0;
        System.out.println("Phase 2 Time elapsed (sec) = " + (System.currentTimeMillis() - startTime) / 1000.0);
        System.out.println("2PMMS time elapsed (sec) = " + total1);
    
        SortUnix sortu = new SortUnix();
        double total2 = sortu.main();
        
        if (total1 > total2)
            System.out.println("System's sort faster by (sec): " + (total1 - total2));
        else
            System.out.println("2PMMS faster by (sec): " + (total2 - total1));
    }

    public static int phase1(int mx, int M, int C) throws Exception {
        int numChunks = 0;
        
        String filename_in = "taxpayers_3M.txt";
        String filename_out = "tempfiles/tempfile";
        BufferedReader in = new BufferedReader(new FileReader(filename_in));
        while (mx > 0) {
            BufferedWriter out = new BufferedWriter(new FileWriter(filename_out+numChunks));
            String[][] chunk = getChunk(in, M, C);
            for (int i = 0; i < M; i++) {
                for (int j = 0; j < 4; j++) {
                    out.write(chunk[i][j] + "\t");
                }
                out.write("\n");
             }
            out.close();
            numChunks++;
            mx--;
        }
        in.close();
        return numChunks;
    }

    //get, sort lines
    public static String[][] getChunk(BufferedReader in, int M, int C) throws Exception {
        int cnt = 0;
        int x = 0;
        String[][] chunk = new String[M][4];
        String temp;
        while (cnt < M) {
            temp = in.readLine();
            chunk[cnt] = temp.split("\t"); 
            cnt++;
        }
        

		Arrays.sort(chunk, new Comparator<String[]>() {
        
            @Override
            public int compare(final String[] entry1, final String[] entry2) {
                final String comp1 = entry1[C];
                final String comp2 = entry2[C];
                return comp1.compareTo(comp2);
            }
        });

        return chunk;
    }
    
    public static void phase2(int n, int B, int C) throws Exception{
        String filename_out = "tempfiles/tempfile";
        BufferedReader[] readers = new BufferedReader[n];
        BufferedWriter writer = new BufferedWriter(new FileWriter("final.txt"), B);
        String[][] curr = new String[n][4];
        for (int i = 0; i < n; i++) {
            readers[i] = new BufferedReader(new FileReader(filename_out+i), B);
            curr[i] = readers[i].readLine().split("\t");
            //System.out.println(curr[i]);
        }
        
        int low = 0;
        
        int x = 0;
        while(true) {
            
            //check for current lowest out of heads 
            for (int i = 0; i < n; i++) {
                if (curr[i] == null)
                    continue;
                low = i;
            }
            //break when finished
            if (curr[low] == null)
                break;
            

            for (int i = 0; i < n; i++) {
                //int temp1, temp2;
                String temp1, temp2;
                if (curr[i] == null)
                    continue;
                
                //check which head to merge
                
                temp1 = curr[i][C];
                temp2 = curr[low][C];
                if (temp1.compareTo(temp2) < 0) {
                    low = i;
                }
                
            }
            
            
            //write lowest to file
            for (int j = 0; j < 4; j++) 
                writer.write(curr[low][j] + "\t");
            writer.write("\n");
             
            //load next element in sublist
            String newString = readers[low].readLine();
            if(newString != null)
                curr[low] = newString.split("\t");
            else
                curr[low] = null;
            
        }

        for (int i = 0; i < n; i++){
            readers[i].close();
        }
        
        writer.close();
    }
    
}
