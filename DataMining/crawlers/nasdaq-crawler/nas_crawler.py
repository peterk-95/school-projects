import time
import csv
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

def init():
    driver = webdriver.Chrome()
    driver.wait = WebDriverWait(driver,5)
    with open('symbols.txt', 'r') as f:
        reader = csv.reader(f)
        y = list(reader)
    return driver, y

def lookup(driver, sym):
    driver.get('http://www.nasdaq.com/symbol/'+sym+'/historical')
    driver.execute_script('var x; $.ajax({type:"POST", url:document.location.href, async:false, data:"5y|false|'+sym+ '", contentType:"application/json", success:function(response){x=response;}, failure:function(response){alert("failed!")}}); document.body.innerHTML=x;')
    elements = driver.find_elements(By.CSS_SELECTOR,'tbody tr td')
    f = open('stock_info/' +sym + '.txt', 'w')
    print ('%s, number of elements: %d' % (sym, len(elements)))
    i = 0
    for ele in elements: 
        i +=1
        f.write(ele.text + '\n')
        if (i % 100) == 0:
            print ('%s, finished: %d, to go: %d' % (sym, i, len(elements)-i))
    print ('--------COMPLETED: %s--------' % sym)
    f.close()

driver, sym_list = init() 


#uncomment forloop to find all companies included in symbols.txt
'''
for comp in sym_list[0]:
    lookup(driver, comp)
'''

#lookup(driver, 'AABA')
#print sym_list
driver.quit()
