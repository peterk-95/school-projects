#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

def compare():
    with open('train_data.txt', 'r') as f:
        tr_orig = f.readlines()    

    with open('test_data.txt', 'r') as f:
        tst_orig = f.readlines()

    with open('out_test.txt', 'r') as f:
        tst_comp = f.readlines()

    with open('out_train.txt', 'r') as f:
        tr_comp = f.readlines()

    tr_cnt = 0
    tst_cnt = 0

    for i in range(len(tr_comp)):
        if tr_orig[i][0] == tr_comp[i][0]: tr_cnt += 1

    for i in range(len(tst_comp)):
        if tst_orig[i][0] == tst_comp[i][0]: tst_cnt += 1

    print 'Training data % correct: ' + str(float(tr_cnt) / len(tr_comp) * 100)
    print 'Test data % correct: ' + str(float(tst_cnt) / len(tst_comp) * 100)
