#!/usr/bin/env python
# -*- coding: utf-8 -*-

#naive-bayes classifier to determine whether a sentence is future tense or past tense
#based on the word choice used in a given line
#train on 'fullltrain.txt', test on 'fulltest.txt'

import math
from math import log
import compare
#tr_labels = open('trainlabels.txt', 'r')
#tr_data = open('traindata.txt', 'r')

with open('test_data.txt', 'r') as f:
    tst_data = f.readlines()    
    f.close()
with open('train_data.txt', 'r') as f:
    tr_data = f.readlines()
    f.close()

n = len(tr_data)

len1, len0 = 0,0

n1, n0 = 0,0

vocablen = 0
Pc1, Pc0 = 0,0
score = [0,0]
features, prob = {}, {}



def extract_features(sentence):
    #check which class current sentence is in
    global n1, n0, len1, len0, vocablen, Pc1, Pc0
    if '1' in sentence:
        n1 += 1
        for v in sentence.split():  
            if v is '1': continue
            if v + '1' not in features and v + '0' not in features: vocablen += 1
            if v + '1' in features: features[v + '1'] += 1
            else: features[v + '1'] = 1 
            len1 += 1
    else: 
        n0 += 1
        for v in sentence.split(): 
            if v is '0': continue 
            if v + '1' not in features and v + '0' not in features: vocablen += 1
            if v + '0' in features: features[v + '0'] += 1
            else: features[v + '0'] = 1 
            len0 += 1
    Pc1 = float(n1) / n
    Pc0 = float(n0) / n


def calc_prob():

    for x in features:
        if '1' in x:
            prob[x] = (float(features[x]) + 1) / (len1 + vocablen ) 
        if '0' in x:
            prob[x] = (float(features[x]) + 1) / (len0 + vocablen )
    
def applyB(sentence): 
    score[1] = log(Pc1, 10)
    score[0] = log(Pc0, 10)
    for x in sentence.split():
        if x is '1' or x is '0': continue

        if x + '1' in prob: score[1] += log(prob[x + '1'], 2)
        else: score[1] += log(1./(len1 + vocablen), 2)

        if x + '0' in prob: score[0] += log(prob[x + '0'], 2)
        else: score[0] += log(1./(len0 + vocablen), 2)

    
    if score[1] is max(score): return 1
    if score[0] is max(score): return 0
    
def main():

    for i in range(n):
        extract_features(tr_data[i])
    calc_prob()
     
    f = open('out_test.txt', 'w')
    for i in range(len(tst_data)):
        result = applyB(tst_data[i][2:])
        to_write = str(result) + ' ' + tst_data[i][2:]
        f.write(to_write)
    f.close()
    

    f = open('out_train.txt', 'w')
    for i in range(len(tr_data)):
        result = applyB(tr_data[i][2:])
        to_write = str(result) + ' ' + tr_data[i][2:]
        f.write(to_write)
    f.close()

main()
compare.compare()
